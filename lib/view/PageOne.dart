import 'package:flutter/material.dart';
import 'package:training2/view/PageTwo.dart';

class PageOne extends StatelessWidget {
  const PageOne({super.key});


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("PageOne"),
        backgroundColor: Colors.deepOrange,
        leading: Icon(Icons.key),
        actions: [
          IconButton(onPressed: ()=>{}, icon: Icon(Icons.person))
        ],
      ),
      body: Center(
      child: ElevatedButton(
        onPressed: ()=>{
          Navigator.push(context,MaterialPageRoute(builder: (context)=> PageTwo(val: 'Hello pwc',)))
        },
        child: Text("Go to new Page"),
      ),
    )
    ) ;
  }
}