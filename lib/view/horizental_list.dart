import 'package:flutter/material.dart';
import 'package:training2/custom_scroll_behavior.dart';

class HorizentalList extends StatelessWidget {
  const HorizentalList({super.key});

  @override
  Widget build(BuildContext context) {
    ScrollController scrollController=ScrollController();
    TextStyle ts=TextStyle(fontSize: 18,color: Colors.white);
    return SizedBox(
      //height: 200,
      child: ScrollConfiguration(
        behavior: MyCustomScrollBehavor(),
        child: ListView(
          controller: scrollController,
      scrollDirection: Axis.horizontal,
      children: [
        Container(
          width: 160,
          color: Colors.red,
          child: Text("1",style: ts,),
        ),
      
        Container(
          width: 160,
          color: Color.fromARGB(255, 20, 196, 58),
          child: Text("2",style: ts,),
        ),
        Container(
          width: 160,
          color: Color.fromARGB(255, 90, 6, 185),
          child: Text("3",style: ts,),
        ),
        Container(
          width: 160,
          color: Color.fromARGB(255, 54, 3, 238),
          child: Text("4",style: ts,),
        ),
        Container(
          width: 160,
          color: Color.fromARGB(255, 238, 4, 199),
          child: Text("5",style: ts,),
        ),
        Container(
          width: 160,
          color: Color.fromARGB(255, 250, 234, 7),
          child: Text("6",style: ts,),
        ),
      ],
    ),
      )
    );
  }
}