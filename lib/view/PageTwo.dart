import 'package:flutter/material.dart';
import 'package:training2/view/horizental_list.dart';
import 'package:training2/view/vartiacal_list.dart';

class PageTwo extends StatelessWidget {
  PageTwo({super.key, required this.val});
  String val="Arpan";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(child: HorizentalList()),
          Expanded(flex:2,  child: VerticalList(),)
         
        ],
      )
    ); 
  }
}