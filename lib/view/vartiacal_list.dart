import 'package:flutter/material.dart';
import 'package:training2/custom_scroll_behavior.dart';

class VerticalList extends StatelessWidget {
  VerticalList({super.key});
  // ignore: non_constant_identifier_names
  List<String> nameList=[
    "Arpan",
    "Arpan2",
    "Arpan3",
    "Arpan4",
    "Arpan5",
    "Arpan6",
    "Arpan7",
    "Arpan8",
    "Arpan9",
    "Arpan9",
    "Arpan9",
    "Arpan9",
    "Arpan9",
    "Arpan9",
    "Arpan9",
    "Arpan9",
    "Arpan9",
    "Arpan9",
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ScrollConfiguration(
        behavior: MyCustomScrollBehavor(),
        child: ListView.builder(
          itemCount: nameList.length,
          itemBuilder: (context, index) => ListTile(
            title: Text(nameList[index]),
          )
        ),
      )
    );
  }
}