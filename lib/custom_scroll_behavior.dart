import 'dart:ui';

import 'package:flutter/material.dart';

class MyCustomScrollBehavor extends MaterialScrollBehavior{
  @override
  // TODO: implement dragDevices
  Set<PointerDeviceKind> get dragDevices => {
      PointerDeviceKind.touch,
      PointerDeviceKind.mouse
  };
}